package com.test.payment.entrypoint.mappers;

import com.test.payment.entrypoint.dto.PaymentResponse;
import com.test.paymentservice.core.entities.Payment;

public final class PaymentResponseMapper {

	public static PaymentResponse toPaymentResponse(Payment payment) {

		return PaymentResponse.builder()
							  .purchaseOrderId(payment.getPaymentId().getValue())
							  .purchaseOrderState(payment.getPaymentState().name())
							  .build();
	}

}
