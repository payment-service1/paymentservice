package com.test.payment.entrypoint.rest.payment;

import com.test.payment.entrypoint.dto.PaymentRequest;
import com.test.payment.entrypoint.dto.PaymentResponse;
import com.test.payment.entrypoint.mappers.PaymentRequestMapper;
import com.test.payment.entrypoint.mappers.PaymentResponseMapper;
import com.test.paymentservice.core.usecases.payment.createpayment.CreatePaymentUseCase;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;

@CrossOrigin
@RestController
@RequestMapping("payment-service/payments")
public class CreatePaymentEndPoint {

	private final CreatePaymentUseCase createPaymentUseCase;

	public CreatePaymentEndPoint(CreatePaymentUseCase createPaymentUseCase) {

		this.createPaymentUseCase = createPaymentUseCase;
	}

	@PostMapping
	public ResponseEntity<PaymentResponse> createPayment(@RequestBody PaymentRequest paymentRequest) {

		var paymentResponse = PaymentResponseMapper
				.toPaymentResponse(createPaymentUseCase.createPayment(PaymentRequestMapper.toPayment(paymentRequest)));
		return Optional.ofNullable(paymentResponse)
					   .map(p -> new ResponseEntity<>(p, HttpStatus.OK))
					   .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
	}

}
