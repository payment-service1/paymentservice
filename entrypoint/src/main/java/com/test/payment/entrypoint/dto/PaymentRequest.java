package com.test.payment.entrypoint.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class PaymentRequest implements Serializable {

	private String purchaseOrderId;
	private Payer payer;
	private CreditCard creditCard;
	private Amount amount;
	private String paymentMethod;

}
