package com.test.payment.entrypoint.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CreditCard implements Serializable {

	private String cardHolderName;
	private String number;
	private String expirationDate;
	private String securityCode;
	private String franchise;

}
