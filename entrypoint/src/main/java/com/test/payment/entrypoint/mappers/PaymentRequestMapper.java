package com.test.payment.entrypoint.mappers;

import com.test.payment.entrypoint.dto.PaymentRequest;
import com.test.paymentservice.core.entities.Amount;
import com.test.paymentservice.core.entities.AmountValue;
import com.test.paymentservice.core.entities.CreditCard;
import com.test.paymentservice.core.entities.CreditCardNumber;
import com.test.paymentservice.core.entities.Email;
import com.test.paymentservice.core.entities.ExpirationDate;
import com.test.paymentservice.core.entities.Payer;
import com.test.paymentservice.core.entities.Payment;
import com.test.paymentservice.core.entities.PaymentId;
import com.test.paymentservice.core.entities.PaymentMethod;

import java.util.UUID;

public final class PaymentRequestMapper {

	public static Payment toPayment(PaymentRequest paymentRequest) {

		return Payment.builder()
					  .paymentId(new PaymentId(paymentRequest.getPurchaseOrderId() != null
											   ? paymentRequest.getPurchaseOrderId()
											   : UUID.randomUUID().toString()))
					  .creditCard(CreditCard.builder()
											.expirationDate(new ExpirationDate(paymentRequest.getCreditCard().getExpirationDate()))
											.franchise(paymentRequest.getCreditCard().getFranchise())
											.creditCardNumber(new CreditCardNumber(paymentRequest.getCreditCard().getNumber(),
																				   paymentRequest.getCreditCard().getFranchise()))
											.cvv(paymentRequest.getCreditCard().getSecurityCode())
											.build())
					  .amount(Amount.builder()
									.currency(paymentRequest.getAmount().getCurrency())
									.amountValue(new AmountValue(paymentRequest.getAmount().getValue()))
									.build())
					  .paymentMethod(PaymentMethod.valueOf(paymentRequest.getPaymentMethod()))
					  .payer(Payer.builder()
								  .email(new Email(paymentRequest.getPayer().getEmail()))
								  .name(paymentRequest.getPayer().getName())
								  .phoneNumber(paymentRequest.getPayer().getPhoneNumber())
								  .build())
					  .build();
	}

}
