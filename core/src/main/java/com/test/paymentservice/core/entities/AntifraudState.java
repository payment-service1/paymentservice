package com.test.paymentservice.core.entities;

public enum AntifraudState {

	PENDING,
	APPROVED,
	REJECTED;
}
