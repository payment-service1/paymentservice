package com.test.paymentservice.core.entities;

import com.test.paymentservice.core.share.ValueObject;
import lombok.EqualsAndHashCode;
import lombok.Getter;

@Getter
@EqualsAndHashCode
public class AmountValue implements ValueObject {

	private final Integer value;

	public AmountValue(Integer amountValue) {

		valid(amountValue);
		this.value = amountValue;
	}

	private void valid(Integer amountValue) {

		if (amountValue == null || amountValue <= 0) {
			throw new IllegalArgumentException("The payment amount should be positive, invalid value:" + amountValue);
		}
	}
}