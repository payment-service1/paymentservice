package com.test.paymentservice.core.utils.creditcardnumber;

import com.test.paymentservice.core.entities.FranchiseType;

public class MastercardNumberValidator extends CreditCardNumberValidator {

	@Override
	public boolean valid(String creditCardNumber, FranchiseType franchiseType) {

		if ( franchiseType == FranchiseType.MASTERCARD && creditCardNumber.startsWith(INIT_MASTERCARD) ) {
			return true;
		} else {
			return this.next.valid(creditCardNumber, franchiseType);
		}
	}

}