package com.test.paymentservice.core.entities;

import com.test.paymentservice.core.share.Entity;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Amount implements Entity {

	private String currency;
	private AmountValue amountValue;

}
