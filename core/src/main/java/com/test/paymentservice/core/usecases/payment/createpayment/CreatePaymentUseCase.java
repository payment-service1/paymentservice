package com.test.paymentservice.core.usecases.payment.createpayment;

import com.test.paymentservice.core.entities.AcquireState;
import com.test.paymentservice.core.entities.AntifraudState;
import com.test.paymentservice.core.entities.PayerId;
import com.test.paymentservice.core.entities.Payment;
import com.test.paymentservice.core.entities.PaymentId;
import com.test.paymentservice.core.entities.PaymentState;
import com.test.paymentservice.core.usecases.creditcard.CreateCreditCardRepository;
import com.test.paymentservice.core.usecases.creditcard.FindCreditCardRepository;
import com.test.paymentservice.core.usecases.payer.CreatePayerRepository;
import com.test.paymentservice.core.usecases.payer.FindPayerRepository;
import com.test.paymentservice.core.usecases.payment.AntifraudRepository;
import com.test.paymentservice.core.usecases.payment.BankRepository;

import java.util.Objects;
import java.util.UUID;

public class CreatePaymentUseCase {

	private final CreatePaymentRepository createPaymentRepository;
	private final FindPayerRepository findPayerRepository;
	private final FindCreditCardRepository findCreditCardRepository;
	private final CreateCreditCardRepository createCreditCardRepository;
	private final CreatePayerRepository createPayerRepository;
	private final AntifraudRepository antifraudRepository;
	private final BankRepository bankRepository;

	public CreatePaymentUseCase(CreatePaymentRepository createPaymentRepository,
								FindPayerRepository findPayerRepository,
								FindCreditCardRepository findCreditCardRepository,
								CreateCreditCardRepository createCreditCardRepository,
								CreatePayerRepository createPayerRepository,
								AntifraudRepository antifraudRepository,
								BankRepository bankRepository) {

		this.createPaymentRepository = createPaymentRepository;
		this.findPayerRepository = findPayerRepository;
		this.findCreditCardRepository = findCreditCardRepository;
		this.createCreditCardRepository = createCreditCardRepository;
		this.createPayerRepository = createPayerRepository;
		this.antifraudRepository = antifraudRepository;
		this.bankRepository = bankRepository;
	}

	public Payment createPayment(Payment payment) {

		var payer = findPayerRepository.findPayerByEmail(payment.getPayer().getEmail().getValue());

		if ( Objects.isNull(payer) ) {
			payment.getPayer().setPayerId(new PayerId(UUID.randomUUID().toString()));
			payment.setPayer(createPayerRepository.createPayer(payment.getPayer()));
		} else {
			payment.setPayer(payer);
		}

		var antifraudState = antifraudRepository.validatePayment(payment.getPayer(), payment.getCreditCard());
		var acquireState = bankRepository.sendPayment(payment.getPayer(), payment.getCreditCard());
		var paymentState = getPaymentState(antifraudState, acquireState);

		var creditCard = findCreditCardRepository.findCreditCardByNumber(payment.getCreditCard().getCreditCardNumber().getValue());
		if ( Objects.isNull(creditCard) ) {
			payment.getCreditCard().setCreditCardId(UUID.randomUUID().toString());
			payment.setCreditCard(createCreditCardRepository.createCreditCard(payment.getCreditCard()));
		} else {
			payment.setCreditCard(creditCard);
		}

		payment.setAntifraudState(antifraudState);
		payment.setAcquireState(acquireState);
		payment.setPaymentState(paymentState);

		return createPaymentRepository.createPayment(payment);
	}

	private PaymentState getPaymentState(AntifraudState antifraudState, AcquireState acquireState) {

		if ( antifraudState == AntifraudState.APPROVED && acquireState == AcquireState.APPROVED) {
			return PaymentState.APPROVED;
		} else if ( antifraudState == AntifraudState.REJECTED || acquireState == AcquireState.REJECTED) {
			return PaymentState.REJECTED;
		} else {
			return PaymentState.PENDING;
		}

	}
}
