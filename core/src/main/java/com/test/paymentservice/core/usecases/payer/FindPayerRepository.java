package com.test.paymentservice.core.usecases.payer;

import com.test.paymentservice.core.entities.Payer;

public interface FindPayerRepository {

	Payer findPayerByEmail(String email);

}
