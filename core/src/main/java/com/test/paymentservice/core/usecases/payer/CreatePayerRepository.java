package com.test.paymentservice.core.usecases.payer;

import com.test.paymentservice.core.entities.Payer;

public interface CreatePayerRepository {

	Payer createPayer(Payer payer);
}
