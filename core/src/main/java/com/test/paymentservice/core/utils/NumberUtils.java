package com.test.paymentservice.core.utils;

public final class NumberUtils {

	public static boolean isNumeric(String string) {
		try {
			Integer.parseInt(string);
			return true;
		} catch (NumberFormatException exception) {
			return false;
		}
	}

}
