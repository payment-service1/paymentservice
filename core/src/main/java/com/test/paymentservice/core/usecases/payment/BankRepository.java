package com.test.paymentservice.core.usecases.payment;

import com.test.paymentservice.core.entities.AcquireState;
import com.test.paymentservice.core.entities.CreditCard;
import com.test.paymentservice.core.entities.Payer;

public interface BankRepository {

	AcquireState sendPayment(Payer payer, CreditCard creditCard);
}
