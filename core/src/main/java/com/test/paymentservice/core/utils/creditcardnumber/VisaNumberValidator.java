package com.test.paymentservice.core.utils.creditcardnumber;

import com.test.paymentservice.core.entities.FranchiseType;

public class VisaNumberValidator extends CreditCardNumberValidator {

	@Override
	public boolean valid(String creditCardNumber, FranchiseType franchiseType) {

		if ( franchiseType == FranchiseType.VISA && creditCardNumber.startsWith(INIT_VISA) ) {
			return true;
		} else {
			return this.next.valid(creditCardNumber, franchiseType);
		}
	}
}
