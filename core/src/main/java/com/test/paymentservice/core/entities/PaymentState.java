package com.test.paymentservice.core.entities;

public enum PaymentState {

	APPROVED,
	REJECTED,
	PENDING;
}
