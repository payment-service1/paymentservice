package com.test.paymentservice.core.entities;

import com.test.paymentservice.core.share.ValueObject;
import lombok.EqualsAndHashCode;
import lombok.Getter;

import java.util.UUID;

@Getter
@EqualsAndHashCode
public class PaymentId implements ValueObject {

	private final String value;

	public PaymentId(String id) {
		ensureValidUuid(id);
		this.value = id;
	}

	private void ensureValidUuid(String id) throws IllegalArgumentException {

		UUID.fromString(id);
	}

}
