package com.test.paymentservice.core.entities;

import com.test.paymentservice.core.share.ValueObject;
import lombok.EqualsAndHashCode;
import lombok.Getter;

import java.util.regex.Pattern;

@Getter
@EqualsAndHashCode
public class Email implements ValueObject {

	private final String value;

	public Email(String email) {
		valid(email);
		this.value = email;
	}

	private void valid(String email) {
		Pattern pattern = Pattern
				.compile("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
								 + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");

		if ( !pattern.matcher(email).matches() ) {
			throw new IllegalArgumentException("The email is not valid: " + email);
		}
	}
}
