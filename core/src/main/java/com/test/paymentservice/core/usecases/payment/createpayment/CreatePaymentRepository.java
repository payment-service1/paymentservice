package com.test.paymentservice.core.usecases.payment.createpayment;

import com.test.paymentservice.core.entities.Payment;

public interface CreatePaymentRepository {

	Payment createPayment(Payment payment);

}
