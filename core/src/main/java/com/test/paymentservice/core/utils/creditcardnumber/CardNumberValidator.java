package com.test.paymentservice.core.utils.creditcardnumber;

import com.test.paymentservice.core.entities.FranchiseType;

public class CardNumberValidator extends CreditCardNumberValidator {

	@Override
	public boolean valid(String creditCardNumber, FranchiseType franchiseType) {

		var visaNumberValidator = new VisaNumberValidator();
		this.setNext(visaNumberValidator);

		var mastercardNumberValidator = new MastercardNumberValidator();
		visaNumberValidator.setNext(mastercardNumberValidator);

		var amexNumberValidator = new AmexNumberValidator();
		mastercardNumberValidator.setNext(amexNumberValidator);

		return next.valid(creditCardNumber, franchiseType);
	}
}
