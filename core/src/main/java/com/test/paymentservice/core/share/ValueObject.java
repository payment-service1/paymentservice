package com.test.paymentservice.core.share;

import java.io.Serializable;

/**
 *  A Domain Driven Design value object
 */
public interface ValueObject extends Serializable {

}
