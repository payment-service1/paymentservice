package com.test.paymentservice.core.utils.creditcardnumber;

import com.test.paymentservice.core.entities.FranchiseType;

public abstract class CreditCardNumberValidator {

	protected CreditCardNumberValidator next;
	protected static final String INIT_VISA = "4";
	protected static final String INIT_MASTERCARD = "5";
	protected static final String INIT_AMEX = "3";

	public void setNext(CreditCardNumberValidator creditCardNumberValidator) {

		this.next = creditCardNumberValidator;
	}

	public CreditCardNumberValidator getNext() {

		return this.next;
	}

	abstract boolean valid(String creditCardNumber, FranchiseType franchiseType);
}
