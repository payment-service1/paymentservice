package com.test.paymentservice.core.entities;

import com.test.paymentservice.core.share.Entity;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Payment implements Entity {

	private PaymentId paymentId;
	private Payer payer;
	private CreditCard creditCard;
	private Amount amount;
	private PaymentMethod paymentMethod;
	private AntifraudState antifraudState;
	private AcquireState acquireState;
	private PaymentState paymentState;
}
