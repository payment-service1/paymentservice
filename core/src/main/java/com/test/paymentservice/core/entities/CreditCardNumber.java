package com.test.paymentservice.core.entities;

import com.test.paymentservice.core.share.ValueObject;
import com.test.paymentservice.core.utils.NumberUtils;
import com.test.paymentservice.core.utils.creditcardnumber.CardNumberValidator;
import lombok.EqualsAndHashCode;
import lombok.Getter;

@Getter
@EqualsAndHashCode
public class CreditCardNumber implements ValueObject {

	private final String value;

	public CreditCardNumber(String cardNumber, String franchise) {
		valid(cardNumber, franchise);
		this.value = cardNumber;
	}

	private void valid(String cardNumber, String franchise) {

		if ( !NumberUtils.isNumeric(cardNumber) && (cardNumber.length() < 14 || cardNumber.length() > 19) ) {
			throw new IllegalArgumentException("The number card is not valid: " + cardNumber);
		}

		var cardNumberValidator = new CardNumberValidator();
		if ( !cardNumberValidator.valid(cardNumber, FranchiseType.valueOf(franchise.toUpperCase())) ) {
			throw new IllegalArgumentException("The credit card number does not correspond to one of our franchises: " +
													   cardNumber);
		}

	}
}
