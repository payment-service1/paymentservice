package com.test.paymentservice.core.utils.creditcardnumber;

import com.test.paymentservice.core.entities.FranchiseType;

public class AmexNumberValidator extends CreditCardNumberValidator {

	@Override
	public boolean valid(String creditCardNumber, FranchiseType franchiseType) {

		return franchiseType == FranchiseType.AMEX && creditCardNumber.startsWith(INIT_AMEX);
	}

}
