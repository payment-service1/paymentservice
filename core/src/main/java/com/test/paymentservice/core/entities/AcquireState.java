package com.test.paymentservice.core.entities;

public enum AcquireState {

	PENDING,
	APPROVED,
	REJECTED;
}
