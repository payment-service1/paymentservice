package com.test.paymentservice.core.entities;

import com.test.paymentservice.core.share.ValueObject;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import org.apache.commons.lang3.StringUtils;

@Getter
@EqualsAndHashCode
public class PayerId implements ValueObject {

	private final String value;

	public PayerId(String id) {
		valid(id);
		this.value = id;
	}

	private void valid(String id) {

		if ( StringUtils.isEmpty(id) ) {
			throw new IllegalArgumentException("The payer identifier should not be empty: " + id);
		}
	}

}
