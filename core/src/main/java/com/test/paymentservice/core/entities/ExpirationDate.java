package com.test.paymentservice.core.entities;

import com.test.paymentservice.core.share.ValueObject;
import lombok.EqualsAndHashCode;
import lombok.Getter;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

@Getter
@EqualsAndHashCode
public class ExpirationDate implements ValueObject {

	private final String value;
	private static final String EXPIRATION_FORMATTER = "MM/yy";

	public ExpirationDate(String expirationDate) {

		valid(expirationDate);
		this.value = expirationDate;
	}

	private void valid(String expirationDate) {

		try {
			var date = new GregorianCalendar();
			var now = new GregorianCalendar();
			date.setTime(new SimpleDateFormat(EXPIRATION_FORMATTER).parse(expirationDate));
			now.setTime(new Date());

			var cardMonth = date.get(Calendar.MONTH);
			var nowMonth = now.get(Calendar.MONTH);
			var cardYear = date.get(Calendar.YEAR);
			var nowYear = now.get(Calendar.YEAR);

			if ( cardYear < nowYear ) {
				throw new IllegalArgumentException("The credit card is expired" + expirationDate);
			}

			if ( cardYear == nowYear && cardMonth < nowMonth ) {
				throw new IllegalArgumentException("The credit card is expired" + expirationDate);
			}

		} catch (final Exception e) {
			throw new IllegalArgumentException("The credit card expiration does not have the format MM/yy: " + expirationDate);
		}


	}
}
