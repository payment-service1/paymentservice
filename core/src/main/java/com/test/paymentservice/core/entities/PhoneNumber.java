package com.test.paymentservice.core.entities;

import com.test.paymentservice.core.share.ValueObject;
import com.test.paymentservice.core.utils.NumberUtils;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import org.apache.commons.lang3.StringUtils;

@Getter
@EqualsAndHashCode
public class PhoneNumber implements ValueObject {

	private final String value;

	public PhoneNumber(String phoneNumber) {

		valid(phoneNumber);
		this.value = phoneNumber;
	}

	private void valid(String phoneNumber) {

		if (StringUtils.isNotEmpty(phoneNumber) && phoneNumber.length() > 10 && !NumberUtils.isNumeric(phoneNumber) ) {
			throw new IllegalArgumentException("The phone number is not valid: " + phoneNumber);
		}
	}

}
