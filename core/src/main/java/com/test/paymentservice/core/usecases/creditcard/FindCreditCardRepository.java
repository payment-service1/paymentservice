package com.test.paymentservice.core.usecases.creditcard;

import com.test.paymentservice.core.entities.CreditCard;

public interface FindCreditCardRepository {

	CreditCard findCreditCardByNumber(String number);

}
