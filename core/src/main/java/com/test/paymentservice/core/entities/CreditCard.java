package com.test.paymentservice.core.entities;

import com.test.paymentservice.core.share.Entity;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class CreditCard implements Entity {

	private String creditCardId;
	private CreditCardNumber creditCardNumber;
	private ExpirationDate expirationDate;
	private String cvv;
	private String franchise;

}
