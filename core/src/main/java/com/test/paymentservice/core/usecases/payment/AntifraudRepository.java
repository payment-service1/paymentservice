package com.test.paymentservice.core.usecases.payment;

import com.test.paymentservice.core.entities.AntifraudState;
import com.test.paymentservice.core.entities.CreditCard;
import com.test.paymentservice.core.entities.Payer;

public interface AntifraudRepository {

	AntifraudState validatePayment(Payer payer, CreditCard creditCard);
}
