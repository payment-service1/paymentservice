package com.test.paymentservice.core.entities;

public enum FranchiseType {

	VISA,
	MASTERCARD,
	AMEX;
}
