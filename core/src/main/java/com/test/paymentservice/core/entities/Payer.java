package com.test.paymentservice.core.entities;

import com.test.paymentservice.core.share.Entity;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Payer implements Entity {

	private PayerId payerId;
	private String name;
	private Email email;
	private String phoneNumber;

}
