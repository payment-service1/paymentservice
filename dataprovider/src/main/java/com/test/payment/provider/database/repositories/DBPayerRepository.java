package com.test.payment.provider.database.repositories;

import com.test.payment.provider.database.entities.PayerDTO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DBPayerRepository extends JpaRepository<PayerDTO, String> {

	PayerDTO findByEmail(String email);

}
