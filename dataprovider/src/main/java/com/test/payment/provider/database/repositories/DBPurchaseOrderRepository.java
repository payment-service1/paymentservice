package com.test.payment.provider.database.repositories;

import com.test.payment.provider.database.entities.PurchaseOrderDTO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DBPurchaseOrderRepository extends JpaRepository<PurchaseOrderDTO, String> {

}
