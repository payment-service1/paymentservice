package com.test.payment.provider.database.entities;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = "PURCHASE_ORDER")
public class PurchaseOrderDTO {

	@Id
	@Basic(optional = false)
	@Column(name = "PURCHASE_ORDER_ID", nullable = false, length = 56)
	private String id;

	@Column(name = "AMOUNT", nullable = false)
	private Integer amount;

	@Column(name = "CURRENCY", nullable = false, length = 56)
	private String currency;

	@Column(name = "PAYMENT_METHOD", nullable = false, length = 56)
	private String paymentMethod;

	@Column(name="ANTIFRAUD_STATE", nullable = false, length = 56)
	private String antifraudState;

	@Column(name="ACQUIRE_STATE", nullable = false, length = 56)
	private String bankState;

	@Column(name="STATE", nullable = false, length = 56)
	private String paymentState;

	@JoinColumn(name = "PAYER_ID", nullable = false)
	@ManyToOne(fetch = FetchType.LAZY)
	private PayerDTO payerDTO;

	@JoinColumn(name = "CREDIT_CARD_ID", nullable = false)
	@ManyToOne(fetch = FetchType.LAZY)
	private CreditCardDTO creditCardDTO;
}
