package com.test.payment.provider.providers.antifraud;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Getter
public class AntifraudResponse {

	private String responseCode;
	private String message;
}
