package com.test.payment.provider.providers.payer;

import com.test.payment.provider.database.entities.PayerDTO;
import com.test.paymentservice.core.entities.Email;
import com.test.paymentservice.core.entities.Payer;
import com.test.paymentservice.core.entities.PayerId;

import java.util.Objects;

public final class PayerMapper {

	public static PayerDTO toPayerDTO(Payer payer) {

		if ( Objects.nonNull(payer) ) {

			return PayerDTO.builder()
						   .id(Objects.nonNull(payer.getPayerId()) ? payer.getPayerId().getValue() : null)
						   .email(payer.getEmail().getValue())
						   .phoneNumber(payer.getPhoneNumber())
						   .build();
		} else {
			return null;
		}
	}

	public static Payer toPayer(PayerDTO payerDTO) {

		if ( Objects.nonNull(payerDTO) ) {

			return Payer.builder()
						.payerId(new PayerId(payerDTO.getId()))
						.email(new Email(payerDTO.getEmail()))
						.phoneNumber(String.valueOf(payerDTO.getPhoneNumber()))
						.build();
		} else {
			return null;
		}
	}

}
