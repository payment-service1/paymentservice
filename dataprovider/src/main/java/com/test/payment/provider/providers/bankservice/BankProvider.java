package com.test.payment.provider.providers.bankservice;

import com.test.paymentservice.core.entities.AcquireState;
import com.test.paymentservice.core.entities.CreditCard;
import com.test.paymentservice.core.entities.Payer;
import com.test.paymentservice.core.usecases.payment.BankRepository;

public class BankProvider implements BankRepository {

	@Override
	public AcquireState sendPayment(Payer payer, CreditCard creditCard) {

		return AcquireState.APPROVED;
	}
}
