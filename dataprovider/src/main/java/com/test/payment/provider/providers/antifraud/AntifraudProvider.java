package com.test.payment.provider.providers.antifraud;

import com.test.paymentservice.core.entities.AntifraudState;
import com.test.paymentservice.core.entities.CreditCard;
import com.test.paymentservice.core.entities.Payer;
import com.test.paymentservice.core.usecases.payment.AntifraudRepository;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.util.HashMap;

public class AntifraudProvider implements AntifraudRepository {

	@Value("${antifraud.url}")
	private String url;
	private static final String END_POINT = "/antifraud-fake-service/validations";
	private final RestTemplate restTemplate;

	public AntifraudProvider(RestTemplate restTemplate) {

		this.restTemplate = restTemplate;
	}

	@Override
	public AntifraudState validatePayment(Payer payer, CreditCard creditCard) {

		var getAntifraudResponse = getAntifraudResponse(payer, creditCard);

		if ( "00".equals(getAntifraudResponse.getResponseCode()) ) {
			return AntifraudState.APPROVED;
		} else if ( "01".equals(getAntifraudResponse.getResponseCode()) ) {
			return AntifraudState.REJECTED;
		} else {
			return AntifraudState.PENDING;
		}
	}

	private AntifraudResponse getAntifraudResponse(Payer payer, CreditCard creditCard) {

		var header = new HttpHeaders();
		header.setContentType(MediaType.APPLICATION_JSON);

		var bodyParamMap = new HashMap<>();
		bodyParamMap.put("cardHolderName", payer.getName());
		bodyParamMap.put("number", creditCard.getCreditCardNumber().getValue());
		bodyParamMap.put("expirationDate", creditCard.getExpirationDate().getValue());
		bodyParamMap.put("securityCode", creditCard.getCvv());
		bodyParamMap.put("franchise", creditCard.getFranchise());

		var response = restTemplate.postForEntity(url+END_POINT, bodyParamMap, AntifraudResponse.class);

		if ( response.getStatusCode() == HttpStatus.OK) {
			return response.getBody();
		} else {
			throw new RuntimeException("Error getting the response from the anti-fraud system");
		}
	}
}
