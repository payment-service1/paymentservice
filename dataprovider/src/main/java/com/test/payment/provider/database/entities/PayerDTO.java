package com.test.payment.provider.database.entities;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = "PAYER")
public class PayerDTO {

	@Id
	@Basic(optional = false)
	@Column(name = "PAYER_ID", nullable = false, length = 56)
	private String id;

	@Column(name = "EMAIL", nullable = false, length = 156)
	private String email;

	@Column(name = "PHONE_NUMBER", nullable = false, length = 56)
	private String phoneNumber;

}
