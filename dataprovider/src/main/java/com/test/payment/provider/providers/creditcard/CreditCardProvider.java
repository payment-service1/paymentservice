package com.test.payment.provider.providers.creditcard;

import com.test.payment.provider.database.repositories.DBCreditCardRepository;
import com.test.paymentservice.core.entities.CreditCard;
import com.test.paymentservice.core.usecases.creditcard.CreateCreditCardRepository;
import com.test.paymentservice.core.usecases.creditcard.FindCreditCardRepository;

import java.util.Objects;

public class CreditCardProvider implements CreateCreditCardRepository, FindCreditCardRepository {

	private final DBCreditCardRepository dbCreditCardRepository;

	public CreditCardProvider(DBCreditCardRepository dbCreditCardRepository) {

		this.dbCreditCardRepository = dbCreditCardRepository;
	}

	@Override
	public CreditCard createCreditCard(CreditCard creditCard) {

		return CreditCardMapper.toCreditCard(dbCreditCardRepository.save(
				Objects.requireNonNull(CreditCardMapper.toCreditCardDTO(creditCard))));
	}

	@Override
	public CreditCard findCreditCardByNumber(String number) {

		return CreditCardMapper.toCreditCard(dbCreditCardRepository.findByNumber(number));
	}
}
