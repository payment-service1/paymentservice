package com.test.payment.provider.providers.payment;

import com.test.payment.provider.database.repositories.DBPurchaseOrderRepository;
import com.test.paymentservice.core.entities.Payment;
import com.test.paymentservice.core.usecases.payment.createpayment.CreatePaymentRepository;

import java.util.Objects;

public class PaymentProvider implements CreatePaymentRepository {

	private final DBPurchaseOrderRepository dbPurchaseOrderRepository;

	public PaymentProvider(DBPurchaseOrderRepository dbPurchaseOrderRepository) {

		this.dbPurchaseOrderRepository = dbPurchaseOrderRepository;
	}

	@Override
	public Payment createPayment(Payment payment) {

		return PaymentMapper.toPayment(dbPurchaseOrderRepository.save(
				Objects.requireNonNull(PaymentMapper.toPurchaseOrderDTO(payment))));
	}
}
