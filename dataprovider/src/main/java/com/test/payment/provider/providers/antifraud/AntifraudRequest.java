package com.test.payment.provider.providers.antifraud;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Builder
public class AntifraudRequest {

	private String cardHolderName;
	private String number;
	private String expirationDate;
	private String securityCode;
	private String franchise;
}
