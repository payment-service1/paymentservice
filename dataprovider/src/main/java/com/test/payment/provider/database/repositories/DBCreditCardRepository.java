package com.test.payment.provider.database.repositories;

import com.test.payment.provider.database.entities.CreditCardDTO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DBCreditCardRepository extends JpaRepository<CreditCardDTO, String> {

	CreditCardDTO findByNumber(String number);

}
