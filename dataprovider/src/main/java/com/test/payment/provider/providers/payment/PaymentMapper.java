package com.test.payment.provider.providers.payment;


import com.test.payment.provider.database.entities.PurchaseOrderDTO;
import com.test.payment.provider.providers.creditcard.CreditCardMapper;
import com.test.payment.provider.providers.payer.PayerMapper;
import com.test.paymentservice.core.entities.AcquireState;
import com.test.paymentservice.core.entities.Amount;
import com.test.paymentservice.core.entities.AmountValue;
import com.test.paymentservice.core.entities.AntifraudState;
import com.test.paymentservice.core.entities.Payment;
import com.test.paymentservice.core.entities.PaymentId;
import com.test.paymentservice.core.entities.PaymentMethod;
import com.test.paymentservice.core.entities.PaymentState;

import java.util.Objects;

public final class PaymentMapper {

	public static PurchaseOrderDTO toPurchaseOrderDTO(Payment payment) {

		if ( Objects.nonNull(payment) ) {

			return PurchaseOrderDTO.builder()
								   .id(payment.getPaymentId().getValue())
								   .amount(payment.getAmount().getAmountValue().getValue())
								   .currency(payment.getAmount().getCurrency())
								   .bankState(payment.getAcquireState().name())
								   .antifraudState(payment.getAntifraudState().name())
								   .paymentState(payment.getPaymentState().name())
								   .paymentMethod(payment.getPaymentMethod().name())
								   .payerDTO(PayerMapper.toPayerDTO(payment.getPayer()))
								   .creditCardDTO(CreditCardMapper.toCreditCardDTO(payment.getCreditCard()))
								   .build();
		} else {
			return null;
		}
	}

	public static Payment toPayment(PurchaseOrderDTO purchaseOrderDTO) {

		if ( Objects.nonNull(purchaseOrderDTO) ) {

			return Payment.builder()
						  .creditCard(CreditCardMapper.toCreditCard(purchaseOrderDTO.getCreditCardDTO()))
						  .paymentId(new PaymentId(purchaseOrderDTO.getId()))
						  .payer(PayerMapper.toPayer(purchaseOrderDTO.getPayerDTO()))
						  .acquireState(AcquireState.valueOf(purchaseOrderDTO.getAntifraudState()))
						  .antifraudState(AntifraudState.valueOf(purchaseOrderDTO.getAntifraudState()))
						  .amount(Amount.builder()
										.amountValue(new AmountValue(purchaseOrderDTO.getAmount()))
										.currency(purchaseOrderDTO.getCurrency())
							 			.build())
						  .paymentMethod(PaymentMethod.valueOf(purchaseOrderDTO.getPaymentMethod()))
						  .paymentState(PaymentState.valueOf(purchaseOrderDTO.getPaymentState()))
						  .creditCard(CreditCardMapper.toCreditCard(purchaseOrderDTO.getCreditCardDTO()))
						  .build();
		} else {
			return null;
		}
	}

}
