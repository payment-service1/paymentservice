package com.test.payment.provider.providers.payer;

import com.test.payment.provider.database.repositories.DBPayerRepository;
import com.test.paymentservice.core.entities.Payer;
import com.test.paymentservice.core.usecases.payer.CreatePayerRepository;
import com.test.paymentservice.core.usecases.payer.FindPayerRepository;

import java.util.Objects;

public class PayerProvider implements CreatePayerRepository, FindPayerRepository {

	private final DBPayerRepository dbPayerRepository;

	public PayerProvider(DBPayerRepository dbPayerRepository) {

		this.dbPayerRepository = dbPayerRepository;
	}

	@Override
	public Payer createPayer(Payer payer) {

		return PayerMapper.toPayer(dbPayerRepository.save(Objects.requireNonNull(PayerMapper.toPayerDTO(payer))));
	}

	@Override
	public Payer findPayerByEmail(String email) {

		return PayerMapper.toPayer(dbPayerRepository.findByEmail(email));
	}
}
