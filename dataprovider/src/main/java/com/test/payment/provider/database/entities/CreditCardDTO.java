package com.test.payment.provider.database.entities;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = "CREDIT_CARD")
public class CreditCardDTO {

	@Id
	@Basic(optional = false)
	@Column(name = "CREDIT_CARD_ID", nullable = false, length = 56)
	private String id;

	@Column(name = "NUMBER", nullable = false, length = 56)
	private String number;

	@Column(name = "EXPIRATION_DATE", nullable = false, length = 30)
	private String expirationDate;

	@Column(name = "FRANCHISE", nullable = false, length = 56)
	private String franchise;

	@Column(name = "SECURITY_CODE", nullable = false, length = 10)
	private String cvv;

}
