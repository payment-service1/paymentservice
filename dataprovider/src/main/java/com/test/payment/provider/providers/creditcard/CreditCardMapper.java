package com.test.payment.provider.providers.creditcard;


import com.test.payment.provider.database.entities.CreditCardDTO;
import com.test.paymentservice.core.entities.CreditCard;
import com.test.paymentservice.core.entities.CreditCardNumber;
import com.test.paymentservice.core.entities.ExpirationDate;

import java.util.Objects;

public final class CreditCardMapper {

	public static CreditCardDTO toCreditCardDTO(CreditCard creditCard) {

		if ( Objects.nonNull(creditCard) ) {

			return CreditCardDTO.builder()
								.id(creditCard.getCreditCardId())
								.number(creditCard.getCreditCardNumber().getValue())
								.expirationDate(creditCard.getExpirationDate().getValue())
								.franchise(creditCard.getFranchise())
								.cvv(creditCard.getCvv())
								.build();
		} else {
			return null;
		}
	}

	public static CreditCard toCreditCard(CreditCardDTO creditCardDTO) {

		if ( Objects.nonNull(creditCardDTO) ) {

			return CreditCard.builder()
							 .creditCardId(creditCardDTO.getId())
							 .creditCardNumber(new CreditCardNumber(creditCardDTO.getNumber(), creditCardDTO.getFranchise()))
							 .franchise(creditCardDTO.getFranchise())
							 .expirationDate(new ExpirationDate(creditCardDTO.getExpirationDate()))
							 .cvv(creditCardDTO.getCvv())
							 .build();
		} else {
			return null;
		}
	}

}
