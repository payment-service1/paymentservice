package com.test.paymentservice.configuration.usecases;

import com.test.paymentservice.core.usecases.creditcard.CreateCreditCardRepository;
import com.test.paymentservice.core.usecases.creditcard.FindCreditCardRepository;
import com.test.paymentservice.core.usecases.payer.CreatePayerRepository;
import com.test.paymentservice.core.usecases.payer.FindPayerRepository;
import com.test.paymentservice.core.usecases.payment.AntifraudRepository;
import com.test.paymentservice.core.usecases.payment.BankRepository;
import com.test.paymentservice.core.usecases.payment.createpayment.CreatePaymentRepository;
import com.test.paymentservice.core.usecases.payment.createpayment.CreatePaymentUseCase;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class PaymentUseCases {

	@Bean
	public CreatePaymentUseCase createPaymentUseCase(CreatePaymentRepository createPaymentRepository,
													 FindPayerRepository findPayerRepository,
													 FindCreditCardRepository findCreditCardRepository,
													 CreateCreditCardRepository createCreditCardRepository,
													 CreatePayerRepository createPayerRepository,
													 AntifraudRepository antifraudRepository,
													 BankRepository bankRepository) {

		return new CreatePaymentUseCase(createPaymentRepository,
										findPayerRepository,
										findCreditCardRepository,
										createCreditCardRepository,
										createPayerRepository,
										antifraudRepository,
										bankRepository);
	}

}
