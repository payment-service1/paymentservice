package com.test.paymentservice.configuration.dataprovider;

import com.test.payment.provider.database.repositories.DBCreditCardRepository;
import com.test.payment.provider.database.repositories.DBPayerRepository;
import com.test.payment.provider.database.repositories.DBPurchaseOrderRepository;
import com.test.payment.provider.providers.antifraud.AntifraudProvider;
import com.test.payment.provider.providers.bankservice.BankProvider;
import com.test.payment.provider.providers.creditcard.CreditCardProvider;
import com.test.payment.provider.providers.payer.PayerProvider;
import com.test.payment.provider.providers.payment.PaymentProvider;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

@Configuration
public class ProviderConfiguration {

	@Bean
	public PaymentProvider paymentProvider(DBPurchaseOrderRepository dbPurchaseOrderRepository) {

		return new PaymentProvider(dbPurchaseOrderRepository);
	}

	@Bean
	public PayerProvider payerProvider(DBPayerRepository dbPayerRepository) {

		return new PayerProvider(dbPayerRepository);
	}

	@Bean
	public CreditCardProvider creditCardProvider(DBCreditCardRepository dbCreditCardRepository) {

		return new CreditCardProvider(dbCreditCardRepository);
	}

	@Bean
	public BankProvider bankProvider() {

		return new BankProvider();
	}

	@Bean
	public AntifraudProvider antifraudProvider(RestTemplate restTemplate) {

		return new AntifraudProvider(restTemplate);
	}

	@Bean
	public RestTemplate restTemplate() {
		return new RestTemplate();
	}
}
