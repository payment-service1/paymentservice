package com.test.paymentservice.configuration;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@Configuration
@EnableAutoConfiguration
@ComponentScan(basePackages = {
		"com.test.paymentservice.configuration"}
)
@EnableJpaRepositories("com.test.payment.provider.database.repositories")
@EntityScan("com.test.payment.provider.database.entities")
public class Application {

	public static void main(String[] args) {

		SpringApplication.run(Application.class, args);
	}

}
