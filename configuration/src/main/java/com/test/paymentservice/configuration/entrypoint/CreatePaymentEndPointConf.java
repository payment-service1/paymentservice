package com.test.paymentservice.configuration.entrypoint;

import com.test.payment.entrypoint.rest.payment.CreatePaymentEndPoint;
import com.test.paymentservice.core.usecases.payment.createpayment.CreatePaymentUseCase;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class CreatePaymentEndPointConf {

	@Bean
	public CreatePaymentEndPoint createPaymentEndPoint(CreatePaymentUseCase createPaymentUseCase) {

		return new CreatePaymentEndPoint(createPaymentUseCase);
	}

}
